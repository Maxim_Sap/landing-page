<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
        <!-- meta charec set -->
        <meta charset="utf-8">
        <!-- Page Title -->
        <title>AE - Responsive HTML Portfolio Template</title>
        <!-- Meta Description -->
        <meta name="description" content="Dark One Page Creative HTML5 Template">
        <meta name="keywords" content="onepage, responsive, parallax, creative, business, html5, css3, css3 animation">
        <meta name="author" content="Karim Ezzat">
	    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="https://www.arbitrage.expert/content/images/client-favicon2.png" rel="shortcut icon" type="image/x-icon">

        <!-- Google Font -->

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>

        <!-- CSS
        ================================================== -->
        <!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <!-- Twitter Bootstrap css -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!--owl slider-->
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <!-- jquery.Magnific popup  -->
        <link rel="stylesheet" href="css/magnific-popup.css">
        <!-- animate -->
        <link rel="stylesheet" href="css/animate.css">
        <!-- flex slider -->
        <link href="css/flexslider.css" rel="stylesheet">
        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="css/main.css">
        <!-- media-queries (Responsive File) -->
        <link rel="stylesheet" href="css/media-queries.css">
		<!-- path slider slick-->
	    <link rel="stylesheet" href="plugins/bootstrap-dialog/bootstrap-dialog.min.css">
	    <link rel="stylesheet" href="plugins/slick/slick.min.css">
	    <link rel="stylesheet" href="css/client.min.css">
	    <link rel="stylesheet" href="css/path.min.css">
        <!-- Modernizer Script for old Browsers -->
        <script src="js/modernizr-2.6.2.min.js"></script>
    </head>
    <body id="body" class="dark">
        <!-- preloader -->
        <div id="preloader" class="dark">
            <div class="loader">
                <span class="loader-inner">Loading...</span>
            </div>
        </div>
        <!-- end preloader -->
        <?php include_once ('parts/header.php'); ?>
        
        <!--
         Why Us/Features Section
         ==================================== -->
<!--        <a name="slider"></a>-->
        <section id="main-slider" class="light-dark">
	        <div class="container">
		        <div class="shadow dark wrapper_first_block slider-box">
			        <!--
					  Home Slider
					  ==================================== -->
					<?php include_once ('parts/slider2.php'); ?>

		        </div>

		        <div class="shadow dark wrapper_first_block text-boxes" style="padding: 20px 20px 10px 20px;">
			        <div class="row">
				        <div class="text-blocks clearfix">
					        <div class="m-col col1"></div>
					        <div class="m-col col2"></div>
					        <div class="texts" style="display: none;">
						        <div class="txt_1_1">
							        <p><i>"Formal education will make you a living; self-education will make you a fortune."</i> - <b>Jim Rohn</b>, entrepreneur, author, and motivational speaker.</p>
							        <p><b>Friendly notice:</b> - Always do your own homework thoroughly, before making any potential high risk financial commitments. Always invest as much as you can afford to lose.</p>
							        <p>Occasionally when the market makes rapid shifts, arbitrage could come forward as very rewarding trading strategy. Being in the right place and at the right time is rather considered an important ingredient to the recipe of successful trading.</p>
							        <p>There are various types of arbitrage strategies that are designed to be executed in different market conditions. During the active Bear market, expert traders would always consider converting a coin back to fiat at the end of each arbitrage trading cycle. Should the market find new lows while you zZz, the investment will continue to remain safe and sound.</p>
							        <p>However, when the bulls are holding the flag, the game of trading suddenly shifts and becomes slightly more predictable as well as  open for non-fiat arbitrage opportunities. In fact, most automated arbitrage trading bots are designed for Bulls market, where buying on exchange  A  and simultaneously selling on exchange B takes place in attempt to increase the overall count of your crypto coins. This particular model is great when the market is constantly rising.</p>
							        <p>Moreover, trusting your coin to third party unregulated company without background check could be frustrating and risky business. While there are plenty of arbitrage bots that can be considered as an automated trading solution, there are certain elements of the trading process that developers are keen to make transparent.
							        </p><p>We consider Arbitrage.expert* a tool* made for identifying arbitrage opportunities in real time across most popular crypto exchanges. <b>It should be used as an addition to an existing trading strategy.</b></p>
							        <p><u>Please be aware:</u> We <b>DO NOT</b> guarantee profits! While some arbitrage opportunities may appear very lucrative at first sight, sometimes there is a good reason behind it. Always do your own research.</p>
							        <ul>
								        <li>Arbitrage Paths is not an arbitrage trading bot. We will never ask for your API keys.</li>
								        <li>According to <a rel="nofollow" href="https://www.google.com/search?ei=IshCW7rdJ9Gn5gLbl67gDQ&amp;q=tool+definition&amp;oq=tool+definition&amp;gs_l=psy-ab.3..35i39k1j0l7j0i203k1.35763.39170.0.39366.10.10.0.0.0.0.78.643.10.10.0....0...1.1.64.psy-ab..0.10.643...0i67k1j0i20i264k1.0.2bWzhaMIhis">Google</a> a “tool” is <i>“a device, used to carry out a particular function”</i>.</li>
							        </ul>
						        </div>

						        <div class="txt_1_2">
							        Think of it as complete and <u>rewarding</u> roadmap across the most popular cryptocurrency exchanges.
						        </div>

						        <div class="txt_1_3">
							        <p>While Expert continually scans the market for wider price spreads, the matching algorithm runs through millions of potential combinations in attempt to display the most accurate real-time arbitrage opportunities.</p>
							        <p>We believe that each <u>path</u> must be carefully validated before it can be followed. The validation process includes confirming the maintenance status of each <b>crypto wallet and</b> ensuring the overall trade+withdrawal fees do not exceed the profit.</p>
						        </div>
						        <div class="txt_2_1">
							        Anyone who can appreciate the art of trading crypto and forex markets. Arbitrage should only be considered as an addition to your existing <a rel="nofollow" href="https://en.wikipedia.org/wiki/Day_trading">day trading</a> strategy. We would strongly encourage to carefully research, evaluate and understand all risks associated with trading cryptocurrency including arbitrage.
						        </div>

						        <div class="txt_2_2">
							        Discover <u>multi-path</u> arbitrage opportunities that occasionally can be difficult to focus your eye on. Arbitrage.expert* is exceptional at scanning the performance of thousands of altcoins across multiple exchanges to identify the most accurate spreads in real-time.
						        </div>

						        <div class="txt_2_3">
							        <p>Cryptocurrency space is very volatile, therefore the accuracy of collected data is crucial for delivery of proper arbitrage quotes. Unlike all other arbitrage apps that are made to primarily rely on [ticker last price], Arbitrage.expert goes far beyond by collecting <u>real-time book data</u> and analyzing each quote individually before concluding a quote.</p>

							        <p>What does it really mean for a day trader?  The last price is simply an indicator of a closing trade, whereas the size still remains an unknown factor. Nevertheless, it does not guarantee an execution of a significantly sized order.  For instance, according to last price a 0.01 BTC <b>ASK</b> order was executed for $6500,  there is absolutely <u>NO guarantee</u> that another <b>ASK</b> order of 1BTC will be identically priced, unless a book is fully scanned and analyzed for potential size matches. As result a BID or multitude of <b>BIDS</b> valued at $6500 are found. This approach ensures the accuracy and highlights experts algorithmic level of sophistication.</p>
						        </div>

					        </div>
				        </div>
			        </div>
		        </div>
	        </div>
        </section>

        <a name="feature"></a>
        <section id="feature">
	        <div >
		        <!-- section title -->
		        <div class="sec-title text-center mb50">
			        <h2 class="color">MEET BUCKS</h2>
			        <div class="devider">
				        <div class="litera-circule">
					        <span>B</span>
				        </div>
			        </div>
		        </div>
		        <!-- / section title -->
		        <!-- fullwidth gray background -->
		        <div class="container"> <!-- container -->
			        <div class="row" role="tabpanel"> <!-- row -->
				        <div class="col-md-5"> <!-- tab menu col 5 -->
					        <ul class="features nav nav-pills nav-stacked meet-bucks" role="tablist">
						        <li role="presentation" class="active">  <!-- feature tab menu #1 -->
							        <a href="#f1" class="shadow" aria-controls="f1" role="tab" data-toggle="tab">
								        <span class="fa simulator"></span>
								        Trading Simulator
							        </a>
						        </li>
						        <li role="presentation"> <!-- feature tab menu #2 -->
							        <a href="#f2" class="shadow" aria-controls="f2" role="tab" data-toggle="tab">
								        <span class="fa trading"></span>
								        Live Mode Trading
							        </a>
						        </li>
						        <li role="presentation"> <!-- feature tab menu #3 -->
							        <a href="#f3" class="shadow" aria-controls="f3" role="tab" data-toggle="tab">
								        <span class="fa alerts"></span>
								        Buy / Sell Alerts
							        </a>
						        </li>
						        <li role="presentation"> <!-- feature tab menu #4 -->
							        <a href="#f4" class="shadow" aria-controls="f4" role="tab" data-toggle="tab">
								        <span class="fa reports"></span>
								        Analytical Reports
							        </a>
						        </li>
						        <li role="presentation"> <!-- feature tab menu #5 -->
							        <a href="#" class="shadow" aria-controls="f5" role="tab" data-toggle="tab">
								        <span class="fa sign-up"></span>
								        Sign Up
							        </a>
						        </li>
					        </ul>
				        </div><!-- end tab menu col 5 -->
				        <div class="col-md-7"> <!-- right content col 6 -->
					        <!-- Tab panes -->
					        <div class="tab-content features-content"> <!-- tab content wrapper -->
						        <div role="tabpanel" class="tab-pane fade in active" id="f1"> <!-- feature #1 content open -->
							        <div class="col-md-12">
								        <!-- flex slider -->
								        <div class="flexslider flexslider-about shadow dark">
									        <ul class="slides">
										        <li>
											        <div class="meet-bucks-inner-box">
												        <h4>Trading Simulator</h4>
												        <p><?=substr('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',0,280);?></p>
												        <div class="meet-bucks-inner-image">
													        <img src="/img/diagrams.png" class="img-responsive" alt="Heydarah">
												        </div>
											        </div>
										        </li>
										        <li>
											        <div class="meet-bucks-inner-box">
												        <h4>Trading Simulator</h4>
												        <p><?=substr('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',0,280);?></p>
												        <div class="meet-bucks-inner-image">
													        <img src="/img/diagrams.png" class="img-responsive" alt="Heydarah">
												        </div>
											        </div>
										        </li>
									        </ul>
								        </div> <!-- end flex slider -->
							        </div>  <!-- end flex slider column -->
						        </div>
						        <div role="tabpanel" class="tab-pane fade" id="f2"> <!-- feature #2 content -->
							        <div class="col-md-12">
								        <!-- flex slider -->
								        <div class="flexslider flexslider-about shadow dark">
									        <ul class="slides">
										        <li>
											        <div class="meet-bucks-inner-box">
												        <h4>Live Mode Trading</h4>
												        <p><?=substr('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',0,280);?></p>
												        <div class="meet-bucks-inner-image">
													        <img src="/img/diagrams.png" class="img-responsive" alt="Heydarah">
												        </div>
											        </div>
										        </li>
										        <li>
											        <div class="meet-bucks-inner-box">
												        <h4>Live Mode Trading</h4>
												        <p><?=substr('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',0,280);?></p>
												        <div class="meet-bucks-inner-image">
													        <img src="/img/diagrams.png" class="img-responsive" alt="Heydarah">
												        </div>
											        </div>
										        </li>
									        </ul>
								        </div> <!-- end flex slider -->
							        </div>  <!-- end flex slider column -->
						        </div>
						        <div role="tabpanel" class="tab-pane fade" id="f3"> <!-- feature #3 content -->
							        <div class="col-md-12">
								        <!-- flex slider -->
								        <div class="flexslider flexslider-about shadow dark">
									        <ul class="slides">
										        <li>
											        <div class="meet-bucks-inner-box">
												        <h4>Buy / Sell Alerts</h4>
												        <p><?=substr('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',0,280);?></p>
												        <div class="meet-bucks-inner-image">
													        <img src="/img/diagrams.png" class="img-responsive" alt="Heydarah">
												        </div>
											        </div>
										        </li>
										        <li>
											        <div class="meet-bucks-inner-box">
												        <h4>Buy / Sell Alerts</h4>
												        <p><?=substr('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',0,280);?></p>
												        <div class="meet-bucks-inner-image">
													        <img src="/img/diagrams.png" class="img-responsive" alt="Heydarah">
												        </div>
											        </div>
										        </li>
									        </ul>
								        </div> <!-- end flex slider -->
							        </div>  <!-- end flex slider column -->
						        </div>
						        <div role="tabpanel" class="tab-pane fade" id="f4"> <!-- feature #4 content -->
							        <div class="col-md-12">
								        <!-- flex slider -->
								        <div class="flexslider flexslider-about shadow dark">
									        <ul class="slides">
										        <li>
											        <div class="meet-bucks-inner-box">
												        <h4>Analytical Reports</h4>
												        <p><?=substr('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',0,280);?></p>
												        <div class="meet-bucks-inner-image">
													        <img src="/img/diagrams.png" class="img-responsive" alt="Heydarah">
												        </div>
											        </div>
										        </li>
										        <li>
											        <div class="meet-bucks-inner-box">
												        <h4>Analytical Reports</h4>
												        <p><?=substr('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',0,280);?></p>
												        <div class="meet-bucks-inner-image">
													        <img src="/img/diagrams.png" class="img-responsive" alt="Heydarah">
												        </div>
											        </div>
										        </li>
									        </ul>
								        </div> <!-- end flex slider -->
							        </div>  <!-- end flex slider column -->
						        </div>
					        </div> <!-- end tab content wrapper -->
				        </div><!-- end right content col 6 -->
			        </div> <!-- end row -->
		        </div> <!-- end container -->
	        </div><!-- end fullwidth gray background -->
        </section>

        <!--
        Spreads Section
        ==================================== -->
        <a name="Spreads"></a>
        <section id="Spreads" class="Spreads light-dark">
	        <div class="container">
                <div class="row">
                    <!-- section title -->
                    <div class="sec-title text-center mb50">
                        <h2 class="color">Spreads</h2>
                        <div class="devider">
		                    <div class="litera-circule">
			                    <span>S</span>
		                    </div>
                        </div>
                    </div>
                    <!-- /section title -->
                    <div class="col-md-12">
                        <!-- flex slider -->
                        <div class="flexslider flexslider-about shadow dark">
                            <ul class="slides">
                                <li>
	                                <div class="flex-caption" style="float: right">
                                        <!-- /put a title here -->
                                        <h3 class="slider-title">Welcome To Spreads</h3>

                                        <!-- Express About Your business -->
                                        <p>Heydarah is a Bootstrap v3.3.5 responsive template .Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type <a href="#" target="_parent">contact us</a>. <br><br>
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type.</p>
	                                    <div class="text-center">
		                                    <a href="#" class="custom-btn transition shadow">Try it free</a>
	                                    </div>

                                    </div>
	                                <div class="img-box-left" style="float: right">
		                                <img src="/img/diagrams.png" alt="Heydarah" /><!-- image of your business  -->
	                                </div>
                                </li>
                            </ul>
                        </div> <!-- end flex slider -->
                    </div>  <!-- end flex slider column -->

                </div> <!-- End of row  -->

            </div><!-- End of Container  -->

        </section> <!-- End of Section  -->


        <!--
        backtesting Section
        ==================================== -->
        <a name="backtesting"></a>
        <section id="backtesting" class="backtesting">
	        <div class="container">
		        <div class="row">
			        <!-- section title -->
			        <div class="sec-title text-center mb50">
				        <h2 class="color">Backtesting</h2>
				        <div class="devider">
					        <div class="litera-circule">
						        <span>B</span>
					        </div>
				        </div>
			        </div>
			        <!-- /section title -->
			        <div class="col-md-12">
				        <!-- flex slider -->
				        <div class="flexslider flexslider-about shadow dark">
					        <ul class="slides">
						        <li>
							        <div class="flex-caption">
								        <!-- /put a title here -->
								        <h3 class="slider-title">Welcome To Backtesting</h3>

								        <!-- Express About Your business -->
								        <p>Heydarah is a Bootstrap v3.3.5 responsive template .Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type <a href="#" target="_parent">contact us</a>. <br><br>
									        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type.</p>
								        <div class="text-center">
									        <a href="#" class="custom-btn transition shadow">Try it free</a>
								        </div>

							        </div>
							        <div class="img-box-right">
							            <img src="/img/diagrams.png" alt="Heydarah" /><!-- image of your business  -->
							        </div>
						        </li>

					        </ul>
				        </div> <!-- end flex slider -->
			        </div>  <!-- end flex slider column -->



		        </div> <!-- End of row  -->

	        </div><!-- End of Container  -->

        </section> <!-- End of Section  -->

        <!--
        arbSimulator Section
        ==================================== -->
        <a name="arbSimulator"></a>
        <section id="arbSimulator" class="arbSimulator light-dark">
	        <div class="container">
		        <div class="row">
			        <!-- section title -->
			        <div class="sec-title text-center mb50">
				        <h2 class="color">Arb Simulator</h2>
				        <div class="devider">
					        <div class="litera-circule">
						        <span>A</span>
					        </div>
				        </div>
			        </div>
			        <!-- /section title -->
			        <div class="col-md-12">
				        <!-- flex slider -->
				        <div class="flexslider flexslider-about shadow dark">
					        <ul class="slides">
						        <li>
							        <div class="flex-caption">
								        <!-- /put a title here -->
								        <h3 class="slider-title">Arb Simulator</h3>
								        <!-- Express About Your business -->
								        <p>Heydarah is a Bootstrap v3.3.5 responsive template .Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type <a href="#" target="_parent">contact us</a>. <br><br>
									        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type.</p>
								        <div class="text-center">
									        <a href="#" class="custom-btn transition shadow">Try it free</a>
								        </div>

							        </div>
							        <div class="img-box-right">
							            <img src="/img/diagrams.png" alt="Heydarah" /><!-- image of your business  -->
							        </div>
						        </li>

					        </ul>
				        </div> <!-- end flex slider -->
			        </div>  <!-- end flex slider column -->

		        </div> <!-- End of row  -->

	        </div><!-- End of Container  -->

        </section> <!-- End of Section  -->

        <!--
        Meet holmes Section
        ==================================== -->
        <a name="holmes"></a>
        <section id="meet-holmes" class="meet-holmes">
	        <div class="container">
		        <div class="row">
			        <!-- section title -->
			        <div class="sec-title text-center mb50">
				        <h2 class="color">Meet Holmes</h2>
				        <div class="devider">
					        <div class="litera-circule">
						        <span>H</span>
					        </div>
				        </div>
			        </div>
			        <!-- /section title -->
			        <div class="col-md-12">
				        <!-- flex slider -->
				        <div class="flexslider flexslider-about shadow dark">
					        <ul class="slides">
						        <li>
							        <div class="flex-caption">
								        <!-- /put a title here -->
								        <h3 class="slider-title">Meet Holmes</h3>
								        <!-- Express About Your business -->
								        <p>Heydarah is a Bootstrap v3.3.5 responsive template .Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type <a href="#" target="_parent">contact us</a>. <br><br>
									        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type.</p>
								        <div class="text-center">
									        <a href="#" class="custom-btn transition shadow">Try it free</a>
								        </div>

							        </div>
							        <div class="img-box-right">
							            <img src="/img/diagrams.png" alt="Heydarah" /><!-- image of your business  -->
							        </div>
						        </li>

					        </ul>
				        </div> <!-- end flex slider -->
			        </div>  <!-- end flex slider column -->

		        </div> <!-- End of row  -->

	        </div><!-- End of Container  -->

        </section> <!-- End of Section  -->

        <!--
        Subscribe Section
        ==================================== -->

        <section id="subscribe" class="subscribe light-dark">
	        <div class="container">
		        <div class="row">

			        <!-- section title -->
			        <div class="sec-title text-center mb50">
				        <h2 class="color">Subscribe</h2>
				        <div class="devider">
					        <div class="litera-circule">
						        <span>S</span>
					        </div>
				        </div>
			        </div>
			        <!-- / section title -->


			        <div id="price-slider">
				        <div class="col-md-3 col-sm-6 col-xs-12">

					        <div class="pricingTable shadow">

						        <h3 class="title dark">1 Month</h3>

						        <div class="pricing-content">

	<!--						        <div class="price-value">$10.00<span class="month">/ month</span></div>-->
							        <ul>
								        <li>50GB Disk Space</li>
								        <li>50 Email Accounts</li>
								        <li>50GB Monthly Bandwidth</li>
								        <li>10 Subdomains</li>
								        <li>15 Domains</li>
							        </ul>
							        <a href="#" class="pricingTable-signup transition shadow">Sing Up</a>
						        </div>
					        </div>
				        </div>
				        <div class="col-md-3 col-sm-6 col-xs-12">
					        <div class="pricingTable shadow">
						        <h3 class="title dark">3 Months</h3>

						        <div class="pricing-content">

	<!--						        <div class="price-value color">$20.00<span class="month">/ month</span></div>-->
							        <ul>
								        <li>60GB Disk Space</li>
								        <li>60 Email Accounts</li>
								        <li>60GB Monthly Bandwidth</li>
								        <li>15 Subdomains</li>
								        <li>20 Domains</li>
							        </ul>
							        <a href="#" class="pricingTable-signup transition shadow">Sing Up</a>
						        </div>

					        </div>
				        </div>
				        <div class="col-md-3 col-sm-6 col-xs-12">
					        <div class="pricingTable shadow">
						        <h3 class="title dark">6 Months</h3>
						        <div class="pricing-content">
	<!--						        <div class="price-value">$40.00<span class="month">/ month</span></div>-->
							        <ul>
								        <li>400GB Disk Space</li>
								        <li>50 Email Accounts</li>
								        <li>50GB Monthly Bandwidth</li>
								        <li>10 Subdomains</li>
								        <li>15 Domains</li>
							        </ul>
							        <a href="#" class="pricingTable-signup transition shadow">Sing Up</a>
						        </div>
					        </div>
				        </div>
				        <div class="col-md-3 col-sm-6 col-xs-12">
					        <div class="pricingTable shadow">
						        <h3 class="title dark">12 Months</h3>
						        <div class="pricing-content">
	<!--						        <div class="price-value">$90.00<span class="month">/ month</span></div>-->
							        <ul>
								        <li>900GB Disk Space</li>
								        <li>50 Email Accounts</li>
								        <li>50GB Monthly Bandwidth</li>
								        <li>10 Subdomains</li>
								        <li>15 Domains</li>
							        </ul>
							        <a href="#" class="pricingTable-signup transition shadow">Sing Up</a>
						        </div>
					        </div>
				        </div>
			        </div>
		        </div>

	        </div>
        </section>
        <!--
	  Starting Blog Section
	  ==================================== -->
        <section id="blog">
	        <div class="container">
		        <div class="row">
			        <!-- section title -->
			        <div class="sec-title text-center mb50">
				        <h2 class="color">Our blog</h2>
				        <div class="devider">
					        <div class="litera-circule">
						        <span>B</span>
					        </div>
				        </div>
			        </div>
			        <!-- / section title -->
			        <div class="col-md-12">
				        <?php require_once ('parts/post_blog.php') ?>
			        </div>
		        </div>
	        </div>
        </section>
        <section id="contact" class="contact light-dark">
            <div class="container">
                <div class="row mb50">
                    <!-- section title -->
                    <div class="sec-title text-center mb50">
                        <h2 class="color">Let’s Discuss</h2>
	                    <div class="devider">
		                    <div class="litera-circule">
			                    <span>D</span>
		                    </div>
	                    </div>
                    </div>
                    <!-- / section title -->
                    <!-- contact address -->
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="contact-address">
<!--                            <h3>Contact INFO!</h3>-->
<!--                            <p>2345 Mansoura Nabaroha, 1234,</p>-->
<!--                            <p>Washington. United States.</p>-->
<!--                            <p>(401) 1234 567</p>-->
                        </div>
                    </div>
                    <!-- end contact address -->
                    <!-- contact form -->
                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                        <div class="contact-form">
<!--                            <h3>Say hello!</h3>-->
                            <form action="#" id="contact-form">
                                <div class="input-group name-email">
                                    <div class="input-field">
                                        <input type="text" name="name" id="name" placeholder="Name" class="form-control">
                                    </div>
                                    <div class="input-field">
                                        <input type="email" name="email" id="email" placeholder="Email" class="form-control">
                                    </div>
                                </div>
                                <div class="input-group">
                                    <textarea name="message" id="message" placeholder="Message" class="form-control"></textarea>
                                </div>
                                <div class="input-group">
                                    <input type="submit" id="form-submit" class="pull-right" value="Send message">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end contact form -->
                    <!-- footer social links -->
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                        <ul class="footer-social">
                            <li><a href="#"><i class="fa fa-behance fa-2x"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter fa-2x"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble fa-2x"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook fa-2x"></i></a></li>
                        </ul>
                    </div>
                    <!-- end footer social links -->
                </div>
            </div>
        </section>
        <!--
        End Contact Section
        ==================================== -->

        <!--
        Footer
        ==================================== -->
		<?php include_once ('parts/footer.php'); ?>
        <!--
        End Footer
        ==================================== -->

        <!--
        Back To Top
        ==================================== -->
        <a href="javascript:void(0);" id="back-top"><i class="fa fa-angle-up fa-3x"></i></a>
        <!--
        End Back To Top
        ==================================== -->

        <!-- Essential jQuery Plugins
        ================================================== -->
        <!-- Main jQuery -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <!-- Single Page Nav -->
        <script src="js/jquery.singlePageNav.min.js"></script>
        <!-- Twitter Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
        <!-- owl slider-->
        <script src="js/owl.carousel.min.js"></script>
        <!-- jquery.Magnific popup -->
        <script src="js/jquery.magnific-popup.min.js"></script>
        <!-- jquery.mixitup.min -->
        <script src="js/jquery.mixitup.min.js"></script>
        <!-- jquery.parallax -->
        <script src="js/jquery.parallax-1.1.3.js"></script>
        <!-- jquery.countTo -->
        <script src="js/jquery-countTo.js"></script>
        <!-- jquery.appear -->
        <script src="js/jquery.appear.js"></script>
        <!-- Contact form validation -->
        <script src="js/jquery.form.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <!-- Google Map -->
<!--        <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
        <!-- jquery easing -->
        <script src="js/jquery.easing.min.js"></script>
        <!-- jquery easing -->
        <script src="js/wow.min.js"></script>
        <!-- flexslider js -->
        <script src="js/jquery.flexslider-min.js"></script>
		<!-- pathSlider-->
        <script src="plugins/bootstrap-dialog/bootstrap-dialog.min.js"></script>
        <script src="js/jquery.star-rating-svg.min.js"></script>
        <script src="js/jquery.blockUI.min.js"></script>
        <script src="js/moment.min.js"></script>
        <script src="js/ag-grid.min.js"></script>
        <script src="js/bootstrap-contextmenu.min.js"></script>
        <script src="js/ag-grid-ext.min.js?ver=202"></script>
        <script src="js/client.min.js?ver=202"></script>
        <script src="js/dttUtils.js"></script>
        <script src="js/mainUtils.js?ver=202"></script>
        <script src="plugins/slick/slick.min.js?v=202"></script>
        <script src="js/pathHelper.min.js"></script>
        <script src="js/pathSlider.min.js"></script>
        <script src="js/highstock.js?v=202"></script>
        <script src="js/chart_spread.min.js"></script>
        <script src="js/fluct_pairs.js?v=202"></script>
        <script src="js/regression.min.js?v=202"></script>
        <script src="js/paths.min.js"></script>
        <!-- Custom Functions -->
        <script src="js/custom.js?t=1"></script>
    </body>
</html>