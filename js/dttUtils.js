window.dttUtils = {
  URLToArray: function(url) {
    if (!url) 
      url = window.location.search;
    if (!url) 
      return {};
    var request = {};
    var arr = [];
    var pairs = url.substring(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < pairs.length; i++) {
      var pair = pairs[i].split('=');

      //check we have an array here - add array numeric indexes so the key elem[] is not identical.
      if(dttUtils.endsWith(decodeURIComponent(pair[0]), '[]') ) {
          var arrName = decodeURIComponent(pair[0]).substring(0, decodeURIComponent(pair[0]).length - 2);
          if(!(arrName in arr)) {
              arr.push(arrName);
              arr[arrName] = [];
          }

          arr[arrName].push(decodeURIComponent(pair[1]));
          request[arrName] = arr[arrName];
      } else {
        var v = decodeURIComponent(pair[1]);
        // if(v.length && v[0]=='"' && v[v.length-1]=='"') {
        //   v = v.substr(1, v.length-2);
        // }
        request[decodeURIComponent(pair[0])] = v;
      }
    }
    return request;
  },

  getParameterByName: function(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  },
  validateIPaddress: function(ipaddress) {  
    if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress)) {  
        return true;
    }  
    return false;  
  },
  roundDbl: function(dbl, places) {
    return Number(dbl.toFixed(places));
  },

  isNullOrUndef: function (variable) {
    return variable === null || variable === undefined;
  },

  // sample use - var ret = (obj, 'part3[0].name')
  resolveObjectFromPath: function (obj, path) {
      if(path === undefined)
          return;
      path = path.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
      path = path.replace(/^\./, '');           // strip a leading dot
      var a = path.split('.');
      for (var i = 0, n = a.length; i < n; ++i) {
          var k = a[i];
          if (k in obj) {
              obj = obj[k];
          } else {
              return;
          }
      }
      return obj;
  },

  getFocusedElement: function() {
      var focused = document.activeElement;
      if (!focused || focused == document.body)
          focused = null;
      else if (document.querySelector)
          focused = document.querySelector(":focus");
      return focused;
  },

  showGeneralError: function(error) {
      BootstrapDialog.alert({
          title: 'Error',
          message: error,
          type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
          closable: false, // <-- Default value is false
          draggable: true, // <-- Default value is false
      });
  },

  size_is_xs: function() {
    return $(window).width() < 768;
  },

  dbUtcToMoment: function(str) {
    return moment(str + "Z");
  },
  dbUtcToStr: function(str, format) {
    if(!format)
      format='YYYY-MM-DD HH:mm:ss';
    return dttUtils.dbUtcToMoment(str).format(format);
  },

  randomInt: function(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //Включно з мінімальним та виключаючи максимальне значення 
  },

  addThousandSeparator: function(val) {
    var str = dttUtils.dblToStrSoft(val);
    var parts = (str + '').split('.'),
        main = parts[0],
        len = main.length,
        output = '',
        i = len - 1;
    
    while(i >= 0) {
        output = main.charAt(i) + output;
        if ((len - i) % 3 === 0 && i > 0) {
            output = ',' + output;
        }
        --i;
    }

    if (parts.length > 1) {
        output += '.' + parts[1];
    }
    return output;
  },
  dblToStrSoft: function (num) {
      if (num == undefined || num == null)
          return num;
      if (num == 0)
          return "0";
      var vDbl = Math.abs(num);
      var ret;
      if (vDbl > 10e4) { ret = num.toFixed(0); }
      else if (vDbl > 999) ret = num.toFixed(0);
      else if (vDbl > 99) ret = num.toFixed(1);
      else if (vDbl > 9) ret = num.toFixed(2);
      else if (vDbl >= 1) ret = num.toFixed(3);
      else if (vDbl > 10e-2) ret = num.toFixed(3);
      else if (vDbl > 10e-3) ret = num.toFixed(4);
      else if (vDbl > 10e-4) ret = num.toFixed(5);
      else if (vDbl > 10e-5) ret = num.toFixed(6);
      else if (vDbl > 10e-6) ret = num.toFixed(7);
      else  ret = num.toFixed(8);
      var idx = ret.indexOf('.');
      if (idx > 0) {
        var idx2;
        for (var i = ret.length - 1; i >= idx; i--) {
            if (ret[i] == '0' || ret[i] == '.') {
                idx2 = i;
                if (ret[i] == '.')
                    break;
            }
            else
                break;
        }
        if (idx2)
            ret = ret.substr(0, idx2);
      }
      return ret;
  },
  roundNumberSoft: function (num) {
    var n = dttUtils.dblToStrSoft(num);
    if(n)
      n = parseFloat(n);
    return n;
  },
  
  valIsString: function(val) {
    return (typeof val === 'string')
  },

  post: function(title, url, data, options) {
    if(options && options.showBlockUI)
      $.blockUI();
    var ret = $.ajax({
      url: url,
      data: data,
      type: "POST", dataType: "json"
    }).fail(function(jqXHR, textStatus, errorThrown) {
      if(jqXHR.status && jqXHR.status == '401') { // Unauthorized 
        location.reload();
        return;
      }
      if(options && options.notShowError)
          return;
      dttUtils.showGeneralError(title + ". General Request failed: textStatus=" + textStatus + ", errorThrown=" + errorThrown);
    }).always(function() {
      if(options && options.showBlockUI)
        $.unblockUI();
    });
    return ret;
  },

  pair_fiats: ['usd','eur','rub','jpy','gbp','cad','chf','cny','aud','usdt'],

  pair_part1: function(pair) {
    var idx = pair.indexOf('/');
    return pair.substr(0,idx);
  },
  pair_part2: function(pair) {
    var idx = pair.indexOf('/');
    return pair.substr(idx+1);
  },
  pair_parts: function(pair) {
    var idx = pair.indexOf('/');
    return {
      p1: pair.substr(0,idx),
      p2: pair.substr(idx+1)
    }
  },

  mirrorPair: function(pair) {
    var idx = pair.indexOf('/');
    return  pair.substr(idx+1) + '/' + pair.substr(0,idx);
  },

  volumeToStr: function(vol, pair, volume_as, showDisabled) {
    if(!showDisabled && volume_as == '0') {
      return "-";
    }
    if(dttUtils.isNullOrUndef(vol))
      return "-";
    var str = "";
    vol = Number(vol);
    if(vol > 1000000)    str = (vol/1000000).toFixed(0) + 'm';
    else if(vol > 1000)  str = (vol/1000).toFixed(0) + 'k';
    else if(vol >= 1)    str = vol.toFixed(vol >= 100 ? 0 : 1);
    else    str = dttUtils.dblToStrSoft(vol);
    if(pair) {
      if(volume_as == '1')
        str += ' ' + pair.substr(0, pair.indexOf('/')).toUpperCase();
      else if(volume_as == '2')
        str += ' ' + pair.substr(pair.indexOf('/')+1).toUpperCase();
      if(showDisabled && volume_as == '0')
        str = '<span style="text-decoration:line-through;">' +str+ '</span>';
    }
    return str;
  },
  BTCtoStr: function(btc) {
    var b = Math.round(btc * 100000000) / 100000000;
    return b.toFixed(8);
  },

  round: function(number, precision) {
    var factor = Math.pow(10, precision);
    var tempNumber = number * factor;
    var roundedTempNumber = Math.round(tempNumber);
    return roundedTempNumber / factor;    
  }, 

  roundUSD: function(usd) {
    return Math.round(usd * 100) / 100;
  },

  roundBTC: function(usd) {
    return dttUtils.round(usd, 8);
  },

  correctInputUSD: function(val) {
    var ret = val.toString()
      .replace(/,/g, '.')
      .replace(/[^0-9\.]/g, '')
      .replace(/^0([\d].+)/, '$1')
      .replace(/(\.\d{2})(.+)/, '$1')
      .replace(/^(0|,|\.|0\.)/, '');
    return ret;
  },
  correctInputBTC: function(val) {
    var ret = val.toString()
      .replace(/[^0-9\.]/g, '')
      .replace(/(\.\d{8})(.+)/, '$1');
    return ret;
  },

  startsWith: function(str, suffix) {
    return str.indexOf(suffix) === 0;
  },
  endsWith: function(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
  },

  localStorage: {
      set: function (key, value) {
          window.localStorage[key] = value;
      },
      get: function (key, defaultValue) {
          return window.localStorage[key] || defaultValue;
      },
      setObject: function (key, value) {
          window.localStorage[key] = JSON.stringify(value);
      },
      getObject: function (key) {
        var ret;
        try{
          ret = JSON.parse(window.localStorage[key] || null);
        } catch {
          ret = null;
        }
        return ret;
      },
      removeItem: function (key) {
          return window.localStorage.removeItem(key);
      },
  },
  
  removeAllPopovers: function() {
    $('body>.popover').remove();
  },

  textWriterOnSymbol: function(opt) {
    //opt.ctrl, txt, endCallback) {
    opt.ctrl.html('<span class="txt"></span><span class="blinking-cursor">|</span>');
    var len = opt.txt.length;
    var idx = 0;
    var ctrlTxt = opt.ctrl.find('.txt');
    var audio;
    if(opt.playSound) {
      audio = new Audio('/sounds/typerwriter.mp3');
      audio.play();
    }
    function do_() {
      idx++;
      if(idx > len || opt.ctrl.closest('body').length == 0) {
        if(audio) {
          audio.pause();
          delete audio;
        }
        opt.ctrl.find('.blinking-cursor').hide();
        opt.endCallback();
        return;
      }
      var str = opt.txt.substr(0, idx);
      ctrlTxt.text(str);
      setTimeout(function(){do_();}, 300);
    }
    do_();
  },
  onAnimateEnd: function(ctrl, callBack) {
    var events = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
    ctrl.one(events, function(){
      ctrl.off(events);
      callBack();
    });
  },
  animatedClassClear: function(ctrl) {
    ctrl.removeClass('animated bounceInUp bounceInDown bounceInRight bounceInLeft bounceOutRight zoomOutDown shake');
  },
  
  actionOnInputNotActiv: function(opt) {
    if(!opt.timeOut || !opt.onNotActive)
      return;
    if(!opt.pr) {
      opt.pr = {};
      opt.ctrl.find('input').on('keydown input change mouseenter focus', function() {
        opt.pr.time = Date.now();
      });
    }
    opt.pr.timeOutMS = opt.timeOut*1000;
    opt.pr.time = Date.now();
    function scanNotActive() {
      if(opt.ctrl.closest('body').length == 0)
        return;
      var now = Date.now();
      if((now - opt.pr.time) > opt.pr.timeOutMS)
        opt.onNotActive(opt);
      if(!opt.pr.stoped)
        opt.pr.timerId = setTimeout(scanNotActive, 5*1000);
    };
    opt.pr.stoped = false;
    opt.pr.timerId = setTimeout(scanNotActive, 5*1000);
    opt.doStop = function() {
      opt.pr.stoped = true;
      clearTimeout(opt.pr.timerId);
    }
  },

}

dttUtils.waitForFinalEvent = (function () {
    var timers = {};
    return function (callback, ms, uniqueId) {
        if (!uniqueId) {
            uniqueId = "Don't call this twice without a uniqueId";
        }
        if (timers[uniqueId]) {
            clearTimeout(timers[uniqueId]);
        }
        timers[uniqueId] = setTimeout(callback, ms);
    };
})();
