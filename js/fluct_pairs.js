'use strict';

+function() {
  var mainUrl = "/client_api?view=fluct_pairs&cmd=";

  appClient.fluctPairs_showAsModal = function(opt) {
    mixpanel.track('reportFluctPairs-open',{});
    
    BootstrapDialog.show({
      size: BootstrapDialog.SIZE_WIDE,
      title: opt.dlgTitle,
      message: $('<div></div>').load('/fluct_pairs'),
      type: BootstrapDialog.TYPE_DEFAULT,
      draggable: true,
      closable: true,
      onshown: function (dialogRef_) {
        var dialog = dialogRef_.getModalDialog();
        dialog.addClass("full-height");
        
        if(!opt.showControls) {
          dialog.find('.show-contrls').html("");
        } else {
          dialog.find('.show-contrls').show();
        }
        showBaseView(opt, dialog.find('.fluct_pairs'));
      }    
    });        
  }

  var extViewPairDays_dlg;
  var extViewPairDay_dlg;
  var showBaseView;

  var exchColors = ['#2f7ed8', '#f28f43'];
  var currExchs;

  function unixDateToDateStr(dt, format) {
    if(!format)
      format = "YYYY-MM-DD";
    if(!dt)
      return dt;
    return moment.utc(dt*1000).format(format);
  }

  function cellRenderer_UnixDate(params) {
    return unixDateToDateStr(params.value);
  }
  function cellRenderer_UnixTime(params) {
    return unixDateToDateStr(params.value, 'HH:mm:ss UTC');
  }
  function cellRenderer_softDbl(params) {
    if(params.value == 0)
      return 0;
    return dttUtils.dblToStrSoft(params.value);
  }
  function percentToStr(perc) {
    return perc.toFixed(2) + '%';
  }
  function cellRenderer_percent(params) {
    return percentToStr(params.value);
  }

  +function() {
    var parent;
    var dtFromUI;
    var dtFromType = dttUtils.localStorage.get('interval.type.1', "week");

    var group_type;

    var pairsInfo;

    var gridBase = {
        enableColResize: true,
        enableFilter: true,
        enableSorting: true,
        suppressMovableColumns: true,
        rowSelection: 'multiple',
        onCellDoubleClicked: function(event) {
          extViewPairDays_dlg(event.data);
        },

        columnDefs: [
          {headerName: "Pair", field: "pair", width: 100, cellStyle: {'text-transform': 'uppercase',}},
          {headerName: "Count intervals", field: "percInfo.cntIntervals", width: 130},
          {
            headerName: "Date from", field: "percByDays.dtFrom", width: 100,
            cellRenderer: cellRenderer_UnixDate,
          },
          {
            headerName: "Date to", field: "percByDays.dtTo", width: 100,
            cellRenderer: cellRenderer_UnixDate,
          },
        ],
    };

    showBaseView = function(opt, parent_) {
      parent = parent_;
      loadInitData(true);

      if(opt.useExchangers)
        currExchs = opt.useExchangers;
      else
        currExchs = dttUtils.localStorage.getObject('selTwoExchange');
      if(!currExchs)
        currExchs = ['gdax','wex.nz'];

      parent.find(`[name="dt_range"][data-type="${dtFromType}"]`).parent().addClass('active');
      dtFromUI_calc(false);

      var tbl = parent.find('#r2-grid');
      new agGrid.Grid(tbl[0], gridBase);
      agGridExt.addCopyMenu(gridBase, tbl);

      parent.find('.commands #btn-view-sel').click(function(){
        var s = gridBase.api.getSelectedNodes();
        if(!s || s.length == 0)
          return alert('no row selected');
        extViewPairDays_dlg(s[0].data);
      });
      
      parent.find('.refreshView input').on('input', refreshView);

      parent.find('[name="dt_range"]').change(function() {
        dtFromUI_calc(true);
      })
    }

    function dtFromUI_calc(doReload) {
      var radio = parent.find('.dt_range-parent .active input');
      var sub_type = radio.data('type');
      var sub_cnt = radio.data('count');
      dtFromUI = moment().utc().startOf('day').subtract(sub_cnt, sub_type);
      if(doReload) {
        dttUtils.localStorage.set('interval.type.1', sub_type);
        loadPairsData();
      }
    }

    function refreshView() {
      var min_perc = parseInt(parent.find('#min_perc').val());
      var durationSec = parseInt(parent.find('#min_time').val()) * 60;
      for (var i = pairsInfo.length - 1; i >= 0; i--) {
        var pi = pairsInfo[i];
        pi.percInfo = {
          cntIntervals: 0,
        };
        if(!pi.percByDays)
          continue;
        var days = pi.percByDays.days;
        for (var idxDay = days.length - 1; idxDay >= 0; idxDay--) {
          var day = days[idxDay];
          day.pair = pi.pair;
          // if(pi.pair == 'btc/eur' && idxDay == 5) {
          //   debugger;
          // }
          var vLast;
          day.percMin = day.avg - (day.avg/100)*min_perc; // minus the specified percent 
          day.arrMinPerc = [];
          var forceAddP = false;
          for (var idxP = 0; idxP < day.arr.length; idxP++) {
            var perc = day.arr[idxP];
            if(!forceAddP && perc.l > day.percMin) {
              vLast = null;
              continue;
            }
            if(vLast) {
              vLast.dtTo = perc.t;
              vLast.duration = vLast.dtTo - vLast.dtFrom;
              vLast.valLast = perc.l;
              vLast.vals.push(perc);
              vLast.cnt++;
              continue;
            }
            // делаем шаг назад
            if(!forceAddP && idxP>0) {
              idxP -= 2;
              forceAddP = true;
              continue;
            }
            forceAddP = false;
            vLast = {
              pair: pi.pair,
              dtFrom:  perc.t,
              dtTo: perc.t,
              valFirst: perc.l,
              valLast: perc.l,
              vals: [perc],
              duration: 0,
              cnt: 1
            };
            day.arrMinPerc.push(vLast);
          }
          day.arrMinPercView = [];
          refreshView_calcViewIntervals_polynomial(day, durationSec);
          pi.percInfo.cntIntervals += day.arrMinPercView.length;
        }
      }
      gridBase.api.setRowData(pairsInfo);
      gridBase.api.sizeColumnsToFit();
    }
    function refreshView_calcViewIntervals_simple(day, durationSec) {
      for (var idxI = 0; idxI < day.arrMinPerc.length; idxI++) {
        var intrv = day.arrMinPerc[idxI];
        if(intrv.duration < durationSec)
          continue;
        day.arrMinPercView.push(intrv);
      }
    }
    function refreshView_calcViewIntervals_polynomial(day, durationSec) {
      for (var idxI = 0; idxI < day.arrMinPerc.length; idxI++) {
        var intrv = day.arrMinPerc[idxI];
        if(intrv.duration < durationSec || intrv.vals.length < 5)
          continue;
        // day.arrMinPercView.push(intrv);
        var arr = [];
        var t0 = intrv.vals[0].t;
        for (var i = 0; i < intrv.vals.length; i++) {
          var perc = intrv.vals[i];
          arr.push([perc.t-t0, perc.l]);
        }
        var res = regression.polynomial(arr, {order: 2, precision: 9,});
        // var ptY_0 = res.points[0][1];
        var firstIdx = 1; // skip first point
        var ptY_0 = res.points[firstIdx][1]; 
        var ptY_Min = ptY_0;
        for (var i = firstIdx; i < res.points.length; i++) {
          var pt = res.points[i];
          var ptY = pt[1];
          if(ptY > ptY_Min) {
            var range = ptY_0 - ptY_Min;
            //var per = (ptY - ptY_Min) * 100 / (ptY_0 - ptY_Min);
            var per = 100 / (ptY / ptY_Min);
            if(per < 95) {
              var perc_i_old = intrv.vals[i];
              var intrvNew = Object.assign(intrv, {
                dtTo: perc_i_old.t,
                valLast: ptY,
                duration: perc_i_old.t - intrv.dtFrom,
                cnt: i+1,
              });
      
              if(intrvNew.duration >= durationSec)
                day.arrMinPercView.push(intrvNew);
              break;
            }
            continue;
          }
          ptY_Min = ptY;
        }
      }
    }

    var initData;
    function loadInitData_Ok() {
      var html = "";
      for (var i = 0; i < initData.exchangers.length; i++) {
        html += "<option>" + initData.exchangers[i] + "</option>";
      }
      parent.find('#exch_1')
        .html(html)
        .css('border-color', exchColors[0])
        .val(currExchs[0]);
      parent.find('#exch_2')
        .html(html)
        .css('border-color', exchColors[1])
        .val(currExchs[1]);
      parent.find('.commands select').change(function() {
        currExchs[0] = parent.find('#exch_1').val();
        currExchs[1] = parent.find('#exch_2').val();
        dttUtils.localStorage.setObject('selTwoExchange', currExchs);
        group_type = parent.find('#group_type').val();
        loadPairsInfo();
      });

      html = "";
      for (var i = 0; i < initData.group_types.length; i++) {
        html += "<option>" + initData.group_types[i] + "</option>";
      }
      group_type = initData.group_types[0];
      parent.find('#group_type')
        .html(html)
        .val(group_type);

      loadPairsInfo();
    }
    function loadInitData() {
      if(initData) {
        return loadInitData_Ok();
      }
      dttUtils.post("report::getInitData", mainUrl + "getInitData", {})
        .done(function(data) {
          if(data.error) {
              dttUtils.showGeneralError("report::getInitData. Error: " + data.error);
              return;
          }

          initData = data;
          loadInitData_Ok();
        });
    }

    function loadPairsInfo() {
      var dataIn = {
        exchangers: currExchs,
        group_type: group_type,
      }

      $.blockUI();
      dttUtils.post("loadPairsInfo", mainUrl + "loadPairsInfo", dataIn)
        .done(function(data) {
          if(data.error) {
              dttUtils.showGeneralError("loadPairsInfo. Error: " + data.error);
              $.unblockUI();
              return;
          }
          // // for test
          // var data2 = [];
          // for (var i = data.length - 1; i >= 0; i--) {
          //   var p = data[i];
          //   if(p.pair=='ltc/btc')
          //     data2.push(p);
          // }
          // data = data2;
          // // for test

          pairsInfo = data;
          loadPairsData();
        });
    }
    function loadPairsData() {
      $.blockUI();
      for (var i = pairsInfo.length - 1; i >= 0; i--) {
        var p = pairsInfo[i];
        p.loaded = undefined;
      }
      loadPairsData_recurs();
    }

    function loadPairsData_recurs() {
      for (var i = pairsInfo.length - 1; i >= 0; i--) {
        var p = pairsInfo[i];
        if(p.loaded) 
          continue;
        var dataIn = {
          idsExchPair: [p.id1, p.id2],
          dtFrom: dtFromUI.unix(),
        }

        dttUtils.post("loadPairsData", mainUrl + "loadPairsData", dataIn)
          .done(function(data) {
            if(data.error) {
              $.unblockUI();
              dttUtils.showGeneralError("loadPairsData. Error: " + data.error);
              gridBase.api.setRowData(pairsInfo);
              return;
            }
            p.loaded = true;
            proccessPairData(p, data);
            loadPairsData_recurs();
          });
        return;
      }
      $.unblockUI();
      refreshView();
    }

    function proccessPairData(pair, data) {
      pair.d1 = [];
      pair.d2 = [];
      for (var i = data.length - 1; i >= 0; i--) {
        var d = data[i];
        if(d.i == pair.id1)
          pair.d1.push(d);
        else if(d.i == pair.id2)
          pair.d2.push(d);
      }
      pair.d1.sort(pairDataSort);
      pair.d2.sort(pairDataSort);

      pair.perc = [];
      if(pair.d1.length>2 && pair.d2.length>2) {
        pairDataCalcPercent(pair.d1, pair.d2, pair.perc);
        pairDataCalcPercent(pair.d2, pair.d1, pair.perc, true);
        pair.perc.sort(pairDataSort);

        // group by days
        pair.percByDays = {
          days: [],
        };
        var percByDayDic = {};
        var daySec = 60*60*24;
        for (var i = 0; i < pair.perc.length; i++) {
          var p = pair.perc[i];
          var tDaySec = ((p.t/daySec)|0) * daySec;
          if(i == 0)
            pair.percByDays.dtFrom = tDaySec;
          pair.percByDays.dtTo = tDaySec;
          var obj = percByDayDic[tDaySec];
          if(!obj) {
            obj = {
              day: tDaySec,
              arr: [],
              sum: 0
            };
            percByDayDic[tDaySec] = obj;
            pair.percByDays.days.push(obj);
          }
          obj.sum += p.l;
          obj.arr.push(p);
        }
        for (var i = pair.percByDays.days.length - 1; i >= 0; i--) {
          var pPerc = pair.percByDays.days[i];
          pPerc.avg = 0;
          if(pPerc.arr.length)
            pPerc.avg = pPerc.sum / pPerc.arr.length;
        }
      }
    }

    function pairDataSort(a,b) {
      if(a.t < b.t) return -1;
      if(a.t > b.t) return 1;
      return 0;
    }

    function pairDataCalcPercent(arr1, arr2, percents, invert) {
      var p2_minIdx = 0;
      var p2_min;
      for (var i1 = 0; i1 < arr1.length; i1++) {
        var p1 = arr1[i1];
        var p1_x = p1.t;
        for (p2_minIdx; p2_minIdx < arr2.length; p2_minIdx++) {
          var p2 = arr2[p2_minIdx];
          var p2_x = p2.t;
          if(p2_x > p1_x)
            break;
          var p2_next = (p2_minIdx+1) < arr2.length ? arr2[p2_minIdx+1] : null;
          if(!p2_next)
            break;
          if(p2_next.t > p1_x) {
            var p2_new = p2;
            if(p2_x != p1_x) {
              // calc point on line 
              var x1 = p2_x;
              var y1 = p2.l;
              var x2 = p2_next.t;
              var y2 = p2_next.l;
              var a = (y2-y1)/(x2-x1);
              var b = (x2*y1-x1*y2)/(x2-x1);
              var y = a * p1_x + b;
              p2_new = {t: p1_x, l: y};
            }
            var perc = dttUtils.roundDbl(100/(p1.l/p2_new.l)-100, 2);
            if(invert)
              perc = perc*-1;
            percents.push({
              t: p1_x, 
              l: perc,
              p1: invert ? p2_new : p1,
              p2: invert ? p1 : p2_new,
            });
            break;
          }
        }
      }
    }    
  }();


  +function() {
    extViewPairDays_dlg = function(data) {
      var dlgData = data;
      BootstrapDialog.show({
        size: BootstrapDialog.SIZE_WIDE,
        title: 'Ext state for <b>"' + dlgData.pair.toUpperCase() + '"</b>' + '<button class="btn btn-default" id="btn-view-sel" style="margin: -5px 0 0 25px;">Select...</button>',
        message: $('<div><div id="r2-grid-ext" class="ag-theme-dark" style="height: 100%;"></div></div>'),
        type: BootstrapDialog.TYPE_DEFAULT,
        draggable: true,
        closable: true,
        onshown: function (dialogRef) {
          var dialog = dialogRef.getModalDialog();
          dialog.addClass("full-height");
    
          var tbl = dialog.find('#r2-grid-ext');
          new agGrid.Grid(tbl[0], gridExt);
          agGridExt.addCopyMenu(gridExt, tbl);
          gridExt.api.setRowData(dlgData.percByDays.days);
          gridExt.api.sizeColumnsToFit();

          dialogRef.getModalHeader().find('#btn-view-sel').click(function(){
            var s = gridExt.api.getSelectedNodes();
            if(!s || s.length == 0)
              return alert('no row selected');
            extViewPairDay_dlg(s[0].data);
          });

          dialog.find('#r2-grid-ext').on('click', 'a.chart', function(){
            var day = $(this).data('day');
            for (var i = 0; i < dlgData.percByDays.days.length; i++) {
              var data = dlgData.percByDays.days[i];
              if(day == data.day) {
                var bands;
                if(data.arrMinPercView.length) {
                  bands = [];
                  for (var i = 0; i < data.arrMinPercView.length; i++) {
                    var intr = data.arrMinPercView[i];
                    bands.push([intr.dtFrom, intr.dtTo]);
                  }
                }

                appClient.chartSpread.showAsModal({
                  dlgTitle: 'BTC/USD Between GDAX & Livecoin Spread Chart.',
                  useExchangers: currExchs,
                  usePair: data.pair,
                  tFrom: data.day,
                  tTo: data.day + 3600*24,
                  bands: bands
                })
                break;
              }
            }
            return false;
          })
        }    
      });        
    }


    var gridExt = {
        enableColResize: true,
        enableFilter: true,
        enableSorting: true,
        suppressMovableColumns: true,
        rowSelection: 'multiple',
        onCellDoubleClicked: function(event) {
          extViewPairDay_dlg(event.data);
        },

        columnDefs: [
          {headerName: "Date", field: "day", width: 100, cellRenderer: cellRenderer_UnixDate},
          {
            headerName: "Avg of day %", field: "avg", width: 150,
            cellRenderer: cellRenderer_percent,
          },
          {
            headerName: "Requested Spread %", field: "percMin", width: 180,
            cellRenderer: cellRenderer_percent,
          },
          {
            headerName: "Count intervals", field: "cnt", width: 130,
            cellRenderer: function(params) {
              return params.data.arrMinPercView.length;
            }
          },
          {
            headerName: "Chart", field: "", width: 130,
            cellRenderer: function(params) {
              // var tTo = params.data.day + 3600*24;
              // var href = `/arb_admnn/rate_chart?ex1=${currExchs[0]}&ex2=${currExchs[1]}&pair=${params.data.pair}&tFrom=${params.data.day}&tTo=${tTo}`;
              // if(params.data.arrMinPercView.length) {
              //   for (var i = 0; i < params.data.arrMinPercView.length; i++) {
              //     var intr = params.data.arrMinPercView[i];
              //     href += `&bands[]=${intr.dtFrom},${intr.dtTo}`;
              //   }
              // }
              // return `<a class="chart" href='${href}' target="_blank">chart</a>`;
              return `<a class="chart" href="#" data-day="${params.data.day}">chart</a>`;
            }
          },
        ],
    };
  }();

  +function() {
    extViewPairDay_dlg = function(data) {
      //arrView
      BootstrapDialog.show({
        size: BootstrapDialog.SIZE_WIDE,
        title: 'Ext state for <b>"' + data.pair.toUpperCase() + '"</b> of <b>"' + unixDateToDateStr(data.day) + '"</b>',
        message: $('<div><div id="r2-grid-ext" class="ag-theme-dark" style="height: 100%;"></div></div>'),
        type: BootstrapDialog.TYPE_DEFAULT,
        draggable: true,
        closable: true,
        onshown: function (dialogRef) {
          var dialog = dialogRef.getModalDialog();
          dialog.addClass("full-height");
    
          var tbl = dialog.find('#r2-grid-ext');
          new agGrid.Grid(tbl[0], gridExt);
          agGridExt.addCopyMenu(gridExt, tbl);
          gridExt.api.setRowData(data.arrMinPercView);
          gridExt.api.sizeColumnsToFit();
        }    
      });        
    }


    var gridExt = {
        enableColResize: true,
        enableFilter: true,
        enableSorting: true,
        suppressMovableColumns: true,
        rowSelection: 'multiple',
        columnDefs: [
          {
            headerName: "Time from", field: "dtFrom", width: 130,
            cellRenderer: cellRenderer_UnixTime,
          },
          {
            headerName: "Time to", field: "dtTo", width: 130,
            cellRenderer: cellRenderer_UnixTime,
          },
          {
            headerName: "Duration", field: "duration", width: 100,
            cellRenderer: function(params) {
              return unixDateToDateStr(params.value, 'HH:mm:ss');
            },
          },
          {
            headerName: "Spread %", field: "dtFrom", width: 200,
            cellRenderer: function(params) {
              return 'from ' + percentToStr(params.data.valFirst) + ' to ' + percentToStr(params.data.valLast);
            }
          },
        ],
    };

  }();  
}();
