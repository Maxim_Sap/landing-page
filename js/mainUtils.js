'use strict';

var mainUtils = {
  fees: {
    typesAll: {
      deposit: {view: "Deposit Fee"},
      withdrawal: {view: "Withdrawal Fee"},
      taker: {view: "Taker Fee"},
      maker: {view: "Maker Fee"},
      market_order: {view: "Market order"},
      limit_order: {view: "Limit order"},
    },
    getTypes: function(exchanger) {
      var all = mainUtils.fees.typesAll;
      if(!mainUtils.fees.typesDef) {
        var keys = Object.keys(all);
        for (var i = 0; i < keys.length; i++) {
          all[keys[i]].name = keys[i];
        }
        mainUtils.fees.typesDef = [all.deposit,all.withdrawal,all.taker,all.maker];
      }
      switch(exchanger) {
        case 'virwox': return [all.deposit,all.withdrawal,all.market_order,all.limit_order];
        case 'quadrigacx': return [all.deposit,all.withdrawal];
        default: return mainUtils.fees.typesDef;
      }
    },
    colsName: {
      deposit: ['Type', 'Fee'],
      withdrawal: ['Type', 'Fee'],
      taker: ['Fee'],
      maker: ['Fee'],
      market_order: ['Market Order','Market Order Fixed','Market Order Variable'],
      limit_order: ['Limit Order','Discount','Effective Commission'],
    },
    colsNameSpecial: {
      'bitfinex': {
        deposit: ['Type', 'Fee', 'Fee Small'],
      },
      'quadrigacx': {
        deposit: ['Type','Transaction Limits', ' 24hr Limit', 'Timeframe', 'Fees', 'Verification'],
        withdrawal: ['Type','Transaction Limits', ' 24hr Limit', 'Timeframe', 'Fees', 'Verification'],
      },
      'nlexch': {
        deposit: ['Currency', 'Confirmations', 'Deposit', 'Network fee', 'Quick Withdraw Max'],
        withdrawal: ['Currency', 'Confirmations', 'Withdraw', 'Network fee', 'Quick Withdraw Max'],
      },
      'btcc': {
        deposit: ['Type', 'Fee', 'Limits'],
        withdrawal: ['Type', 'Fee', 'Limits'],
      },
      'hitbtc': {
        withdrawal: ['Type', 'Fee', 'Fee in USD'],
      },
      'xbtce': {
        deposit: ['Type', 'Fee', 'Minimum amount'],
        withdrawal: ['Type', 'Fee', 'Min. AMT.'],
        taker: ['Fee', 'Volume'],
        maker: ['Fee', 'Volume'],
      },
      'coinsbank': {
        deposit: ['Type', 'Fee', 'Limits'],
        withdrawal: ['Type', 'Fee', 'Limits'],
      },
      'poloniex': {
        taker: ['Fee', 'Volume'],
        maker: ['Fee', 'Volume'],
      },
      'getbtc.org': {
        taker: ['Fee', 'Volume'],
        maker: ['Fee', 'Volume'],
      },
      'lakebtc': {
        taker: ['Fee', 'Volume'],
        maker: ['Fee', 'Volume'],
      },
    },

    getColsName: function(exchanger, type) {
      var cn = mainUtils.fees.colsName[type];
      if(mainUtils.fees.colsNameSpecial[exchanger] && mainUtils.fees.colsNameSpecial[exchanger][type])
        cn = mainUtils.fees.colsNameSpecial[exchanger][type];
      return cn;
    },
  },
  feesLink: {
    'bitfinex': 'https://www.bitfinex.com/fees',
    'yobit': 'https://yobit.net/en/fees/',
    'kraken': 'https://www.kraken.com/help/fees',
    'livecoin': 'https://www.livecoin.net/fees',
    'coinbase': 'https://support.coinbase.com/customer/en/portal/articles/2109597-coinbase-pricing-fees-disclosures',
    'cex.io': 'https://cex.io/fee-schedule',
    'bitstamp': 'https://www.bitstamp.net/fee_schedule/',
    'gemini': 'https://gemini.com/fee-schedule/',
    'gdax': 'https://www.gdax.com/fees/BTC-USD',
    'itbit': 'https://www.itbit.com/fees',
    'localbitcoins': 'https://localbitcoins.com/fees',
    'binance': 'https://support.binance.com/hc/en-us/articles/115000429332-Fee-Structure-on-Binance',
    'vip.bitcoin': 'https://help.bitcoin.co.id/bagaimana-rincian-fee-di-vip-bitcoin-co-id/',
    'bleutrade': 'https://bleutrade.com/help/fees_and_deadlines',
    'btc.markets': 'https://www.btcmarkets.net/fees',
    'bittrex': 'https://bittrex.com/fees',
    '??bx': 'https://bx.in.th/info/fees/',
    'therocktrading': 'https://therocktrading.com/en/pages/fees',
    'gatecoin': 'https://support.gatecoin.com/hc/en-us/articles/115008205267-Overview-of-trading-fees-on-Gatecoin',
    'hitbtc': 'https://hitbtc.com/fees-and-limits',
    'liqui': 'https://liqui.io/fee',
    'okex': 'https://www.okex.com/fees.html',
    'poloniex': 'https://poloniex.com/fees/',
    'virwox': 'https://www.virwox.com/help.php#_Fees',
    'quoine': 'https://quoine.zendesk.com/hc/en-us/articles/115011281488-Fees',
    'xbtce': 'https://support.xbtce.info/Knowledgebase/Article/View/132/7/fees',
    'getbtc.org': 'https://getbtc.org/fee-schedule.php',
    'coinsbank': 'https://coinsbank.com/fees',
    'btcc': 'https://www.btcc.com/fees',
    'wex.nz': 'https://wex.nz/fees',
    'lakebtc': 'https://www.lakebtc.com/s/fees?locale=en',
    'kucoin': 'https://news.kucoin.com/en/fee/',
    'exmo': 'https://exmo.com/en/docs/fees',
    'quadrigacx': 'https://www.quadrigacx.com/account-funding-withdrawal',
    'idex': 'https://idex.market/faq',
    'nlexch': 'https://www.nlexch.com/fees',
    'bit-z': 'https://www.bit-z.com/about/fee',
    'huobi': 'https://www.huobi.pro/about/fee/',
  },
};
