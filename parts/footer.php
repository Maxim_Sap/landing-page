<footer id="footer" class="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="footer-single">
					<h6>Subscribe </h6>
					<form action="#" class="subscribe">
						<input type="text" name="subscribe" id="subscribe">
						<input type="submit" value="&#8594;" id="subs" class="color">
					</form>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
				</div>
			</div>
			<div class="col-md-9 col-sm-9 col-xs-12">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-2 col-sm-4 col-xs-4">
						<div class="footer-single">
							<h6>Exchanges</h6>
							<ul>
								<li><a href="#">Binance</a></li>
								<li><a href="#">Bittrex</a></li>
								<li><a href="#">Coinbase Pro</a></li>
								<li><a href="#">HITBTC</a></li>
								<li><a href="#">Kraken</a></li>
								<li><a href="#">Livecoin</a></li>
								<li><a href="#">BITMEX</a></li>
								<li><a href="#">HUOBI</a></li>
							</ul>
						</div>
					</div>

					<div class="col-md-2 col-sm-4 col-xs-4">
						<div class="footer-single">
							<h6>Company</h6>
							<ul>
								<li><a href="#">Newsletter</a></li>
								<li><a href="#">Contact Us</a></li>
								<li><a href="#">About Us</a></li>
								<li><a href="#">Donate</a></li>
								<li><a href="#">Telegram</a></li>
								<li><a target="_blank" href="https://www.facebook.com/arbexp/">Facebook</a></li>
								<li><a target="_blank" href="https://twitter.com/arb_exp">Twitter</a></li>
								<li><a href="#">Bitcointalk</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 col-sm-4 col-xs-4">
						<div class="footer-single">
							<h6>Free Tools</h6>
							<ul>
								<li><a target="_blank" href="https://www.arbitrage.expert/">Arbitrage</a></li>
								<li><a href="#main-slider">Paths</a></li>
								<li><a href="#Spreads">Spreads</a></li>
								<li><a href="#backtesting">Backtesting</a></li>
								<li><a href="#holmes">Holmes</a></li>
								<li><a href="#">Wallet Status</a></li>
								<li><a href="#meet-bucks" onclick=$('a[href="#f3"]').click();>Alerts</a></li>
								<li><a href="#arbSimulator">WP Arb Plugin</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 col-sm-4 col-xs-4">
						<div class="footer-single">
							<h6>Bots</h6>
							<ul>
								<li><a href="#meet-bucks">Bucks</a></li>
								<li><a href="#">Cartman</a></li>
								<li><a href="#">Sunny</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 col-sm-4 col-xs-4">
						<div class="footer-single">
							<h6>Links</h6>
							<ul>
								<li><a href="#">Wikis</a></li>
								<li><a href="#">Privacy Policy</a></li>
								<li><a href="#">Terms of Service</a></li>
								<li><a href="#">Refund Policy</a></li>
								<li><a href="#">Affiliates</a></li>
								<li><a href="#">Our blog</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<p class="copyright text-center">
					Copyright © 2018-<?=date("Y")?> Arbitrage.expert. All rights reserved.
				</p>
			</div>
		</div>
	</div>
</footer>