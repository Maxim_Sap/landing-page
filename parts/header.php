<!--
        Fixed Navigation
        ==================================== -->
<header id="navigation" class="parent_menu navbar-fixed-top navbar">
	<div class="container">
		<div class="navbar-header">
			<!-- responsive nav button -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<i class="fa fa-bars fa-2x color"></i>
			</button>
			<!-- /responsive nav button -->

			<!-- logo -->
			<a class="navbar-brand" href="#body">
				<h1 id="logo">AE</h1>
<!--				<img src="img/logo.png" alt="Heydarah">-->
			</a>
			<!-- /logo -->
		</div>

		<!-- main nav -->
		<nav class="collapse navbar-collapse navbar-right">
			<ul id="nav" class="nav navbar-nav">
				<li>
					<a href="#main-slider">Paths</a>
				</li>
				<li>
					<a href="#feature">Bucks</a>
					<span class="divider"></span>
				</li>
				<li>
					<a href="#Spreads">Spreads</a>
					<span class="divider"></span>
				</li>
				<li>
					<a href="#backtesting">Backtesting</a>
					<span class="divider"></span>
				</li>
				<li>
					<a href="#arbSimulator">Arb Simulator</a>
					<span class="divider"></span>
				</li>
				<li>
					<a href="#meet-holmes">Holmes</a>
					<span class="divider"></span>
				</li>
				<li>
					<a href="#subscribe">Pricing</a>
					<span class="divider"></span>
				</li>
				<li>
					<a href="#blog">Blog</a>
					<span class="divider"></span>
				</li>
				<li>
					<a target="_blank" href="https://www.arbitrage.expert/">Arbitrage</a>
					<span class="divider"></span>
				</li>
				<li>
					<a href="#contact">Contact</a>
					<span class="divider"></span>
				</li>
				<li>
					<a href="#contact">Log in</a>
					<span class="divider"></span>
				</li>
				<li>
					<a href="#contact">Sign up</a>
					<span class="divider"></span>
				</li>
			</ul>
		</nav>
		<!-- /main nav -->
	</div>
</header>
<!--
End Fixed Navigation
==================================== -->