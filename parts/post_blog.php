<div id="news-slider" class="owl-carousel">
	<div class="post-slide light-dark">
		<div class="post-img">
			<img src="img/blog/blog_def.pdf" alt="Heydarah">

<!--			<div class="post-date">-->
<!--				<span class="date dark">3</span>-->
<!--				<span class="month light-dark shadow color">Feb</span>-->
<!--			</div>-->
		</div>
		<div class="post-content">
			<h3 class="post-title">
				<a href="https://www.arbitrage.expert/a-arbitrage-trading-has-potential-in-making-hefty-sums">Arbitrage trading has potential</a>
			</h3>
			<p class="post-description">
				The idea of arbitrage trading has held many folks a merchant’s fixation, especially when it can be done over and over again. Arbitrage, ...
			</p>
			<a href="https://www.arbitrage.expert/a-arbitrage-trading-has-potential-in-making-hefty-sums" class="read-more">read more</a>
		</div>
	</div>
	<div class="post-slide light-dark">
		<div class="post-img">
			<img src="img/blog/blog_def.pdf" alt="Heydarah">

<!--			<div class="post-date">-->
<!--				<span class="date dark">5</span>-->
<!--				<span class="month light-dark shadow color">Jan</span>-->
<!--			</div>-->
		</div>
		<div class="post-content">
			<h3 class="post-title">
				<a href="https://www.arbitrage.expert/a-the-best-bitcoin-exchanges">The Best Bitcoin Exchanges</a>
			</h3>
			<p class="post-description">
				Every time it comes to digging thru to find some of the best bitcoin exchange, it may not be as straightforward as it sounds for opened  ...
			</p>
			<a href="https://www.arbitrage.expert/a-the-best-bitcoin-exchanges" class="read-more">read more</a>
		</div>
	</div>
	<div class="post-slide light-dark">
		<div class="post-img">
			<img src="/img/blog/gdax.jpg" alt="Heydarah">
<!--			<div class="post-date">-->
<!--				<span class="date dark">5</span>-->
<!--				<span class="month light-dark shadow color">Apr</span>-->
<!--			</div>-->
		</div>
		<div class="post-content">
			<h3 class="post-title">
				<a href="https://www.arbitrage.expert/a-gdax-the-coinbase-buddy">GDAX: the Сoinbase buddy</a>
			</h3>
			<p class="post-description">
				The coinbase corporation was founded in 2011 by Brian Armstrong and Fred Ehrsam. In 2012 coinbase opened its doors for business ...
			</p>
			<a href="https://www.arbitrage.expert/a-gdax-the-coinbase-buddy" class="read-more">read more</a>
		</div>
	</div>
	<div class="post-slide light-dark">
		<div class="post-img">
			<img src="/img/blog/cex.io.jpg" alt="Heydarah">
<!--			<div class="post-date">-->
<!--				<span class="date dark">5</span>-->
<!--				<span class="month light-dark shadow color">Mar</span>-->
<!--			</div>-->
		</div>
		<div class="post-content">
			<h3 class="post-title">
				<a href="https://www.arbitrage.expert/a-comprehensive-cex-review-whats-all-that-noise-about">Comprehensive CEX Review: What’s all that noise about</a>
			</h3>
			<p class="post-description">
				I begin again by stressing the importance of security aspects any user must address prior to registering and making any trades...
			</p>
			<a href="https://www.arbitrage.expert/a-comprehensive-cex-review-whats-all-that-noise-about" class="read-more">read more</a>
		</div>
	</div>
	<div class="post-slide light-dark">
		<div class="post-img">
			<img src="/img/blog/counbase.jpg" alt="Heydarah">
<!--			<div class="post-date">-->
<!--				<span class="date dark">5</span>-->
<!--				<span class="month light-dark shadow color">Mar</span>-->
<!--			</div>-->
		</div>
		<div class="post-content">
			<h3 class="post-title">
				<a href="https://www.arbitrage.expert/a-coinbase-safe-haven-or-just-another-blah-blah">Coinbase: safe haven or just another blah blah…</a>
			</h3>
			<p class="post-description">
				I don’t think everyone had a chance to become familiar with a Coinbase review that completely evaluates the company's business philosophy ...
			</p>
			<a href="https://www.arbitrage.expert/a-coinbase-safe-haven-or-just-another-blah-blah" class="read-more">read more</a>
		</div>
	</div>
	<div class="post-slide light-dark">
		<div class="post-img">
			<img src="/img/blog/bittrex.jpg" alt="Heydarah">
<!--			<div class="post-date">-->
<!--				<span class="date dark">5</span>-->
<!--				<span class="month light-dark shadow color">Mar</span>-->
<!--			</div>-->
		</div>
		<div class="post-content">
			<h3 class="post-title">
				<a href="https://www.arbitrage.expert/a-what-is-bittrex-and-who-stands-behind-it">What is Bittrex and who stands behind it?</a>
			</h3>
			<p class="post-description">
				From the messy scandal resulting in big outflow of Poloniex active users, to shutdown of BTCE by the US justice, raises  ...
			</p>
			<a href="https://www.arbitrage.expert/a-what-is-bittrex-and-who-stands-behind-it" class="read-more">read more</a>
		</div>
	</div>
	<div class="post-slide light-dark">
		<div class="post-img">
			<img src="/img/blog/binance.jpg" alt="Heydarah">
<!--			<div class="post-date">-->
<!--				<span class="date dark">5</span>-->
<!--				<span class="month light-dark shadow color">Mar</span>-->
<!--			</div>-->
		</div>
		<div class="post-content">
			<h3 class="post-title">
				<a href="https://www.arbitrage.expert/a-binance-review-2018-bitcoin-exchange">Binance Review 2018 - Bitcoin Exchange</a>
			</h3>
			<p class="post-description">
				Binance has earned its popularity by making it to the list of the world’s top three biggest exchanges. In this comprehensive Binance review ...
			</p>
			<a href="https://www.arbitrage.expert/a-binance-review-2018-bitcoin-exchange" class="read-more">read more</a>
		</div>
	</div>
	<div class="post-slide light-dark">
		<div class="post-img">
			<img src="/img/blog/localBitcoin.jpg" alt="Heydarah">
<!--			<div class="post-date">-->
<!--				<span class="date dark">5</span>-->
<!--				<span class="month light-dark shadow color">Mar</span>-->
<!--			</div>-->
		</div>
		<div class="post-content">
			<h3 class="post-title">
				<a href="https://www.arbitrage.expert/a-localbitcoins-review">Localbitcoins Review</a>
			</h3>
			<p class="post-description">
				We all remember the period when cryptocurrency had started gaining momentum, many folks were beginning to consider crypto as potential long term investment vehicle. ...
			</p>
			<a href="https://www.arbitrage.expert/a-localbitcoins-review" class="read-more">read more</a>
		</div>
	</div>
	<div class="post-slide light-dark">
		<div class="post-img">
			<img src="/img/blog/kraken.jpg" alt="Heydarah">
<!--			<div class="post-date">-->
<!--				<span class="date dark">5</span>-->
<!--				<span class="month light-dark shadow color">Mar</span>-->
<!--			</div>-->
		</div>
		<div class="post-content">
			<h3 class="post-title">
				<a href="https://www.arbitrage.expert/a-kraken-comprehensive-review">Kraken comprehensive review</a>
			</h3>
			<p class="post-description">
				Kraken is known to be one of the most liquid Bitcoin exchange by volume in Euro. The popular exchange was founded in 2011 by Jesse Powell  ...
			</p>
			<a href="https://www.arbitrage.expert/a-kraken-comprehensive-review" class="read-more">read more</a>
		</div>
	</div>
	<div class="post-slide light-dark">
		<div class="post-img">
			<img src="/img/blog/bitstamp.jpg" alt="Heydarah">
<!--			<div class="post-date">-->
<!--				<span class="date dark">5</span>-->
<!--				<span class="month light-dark shadow color">Mar</span>-->
<!--			</div>-->
		</div>
		<div class="post-content">
			<h3 class="post-title">
				<a href="https://www.arbitrage.expert/a-comprehensive-bitstamp-review">Comprehensive Bitstamp Review</a>
			</h3>
			<p class="post-description">
				In this comprehensive review, will will touch upon some of the most important details about Bitstamp’s impressive six years of rapid growth.  ...
			</p>
			<a href="https://www.arbitrage.expert/a-comprehensive-bitstamp-review" class="read-more">read more</a>
		</div>
	</div>
	<div class="post-slide light-dark">
		<div class="post-img">
			<img src="img/blog/blog_def.pdf" alt="Heydarah">
<!--			<div class="post-date">-->
<!--				<span class="date dark">5</span>-->
<!--				<span class="month light-dark shadow color">Mar</span>-->
<!--			</div>-->
		</div>
		<div class="post-content">
			<h3 class="post-title">
				<a href="https://www.arbitrage.expert/a-yobit-exchange-comprehensive-review">The YoBit Exchange Comprehensive Review – Scam or Not?</a>
			</h3>
			<p class="post-description">
				The most recent buzzes around the tech industry is investing in Cryptocurrency There is most definitely a lot of risk  ...
			</p>
			<a href="https://www.arbitrage.expert/a-yobit-exchange-comprehensive-review" class="read-more">read more</a>
		</div>
	</div>
	<div class="post-slide light-dark">
		<div class="post-img">
			<img src="img/blog/gemini.jpg" alt="Heydarah">
<!--			<div class="post-date">-->
<!--				<span class="date dark">5</span>-->
<!--				<span class="month light-dark shadow color">Mar</span>-->
<!--			</div>-->
		</div>
		<div class="post-content">
			<h3 class="post-title">
				<a href="https://www.arbitrage.expert/a-gemini-comprehensive-review">Gemini Comprehensive Review 2018</a>
			</h3>
			<p class="post-description">
				The Gemini cryptocurrency exchange incorporated sometime in 2015 by the two twin brothers Cameron and Tyler Winklevoss ...
			</p>
			<a href="https://www.arbitrage.expert/a-gemini-comprehensive-review" class="read-more">read more</a>
		</div>
	</div>
	<div class="post-slide light-dark">
		<div class="post-img">
			<img src="img/blog/bestBit.jpg" alt="Heydarah">
<!--			<div class="post-date">-->
<!--				<span class="date dark">5</span>-->
<!--				<span class="month light-dark shadow color">Mar</span>-->
<!--			</div>-->
		</div>
		<div class="post-content">
			<h3 class="post-title">
				<a href="https://www.arbitrage.expert/a-the-easy-way-for-choosing-best-bitcoin-wallet">The easy way for choosing best Bitcoin wallet</a>
			</h3>
			<p class="post-description">
				How about the sad Bitcoin stories on social media channels like Reddit and Facebook catching everyone’s attention? Ye ...
			</p>
			<a href="https://www.arbitrage.expert/a-the-easy-way-for-choosing-best-bitcoin-wallet" class="read-more">read more</a>
		</div>
	</div>
</div>