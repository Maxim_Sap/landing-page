<section id="slider">
	<a name="main-slider"></a>
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

		<!-- Indicators bullet -->
<!--		<ol class="carousel-indicators">-->
<!--			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>-->
<!--			<li data-target="#carousel-example-generic" data-slide-to="1"></li>-->
<!--		</ol>-->
		<!-- End Indicators bullet -->

		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">

			<!-- single slide -->
			<div class="item active" style="background-image: url('/img/Screenshot_7.png');">
				<div class="carousel-caption">

					<!-- You can add a tittle here that describe your business -->
<!--					<h2 data-wow-duration="700ms" data-wow-delay="500ms" class="wow bounceInDown animated"><span> </span><br></h2>-->
<!--					<h3 data-wow-duration="1000ms" class="wow slideInLeft animated"><span class="color">/creative</span> one page template.</h3>-->
<!--					<p data-wow-duration="1000ms" class="wow slideInRight animated">We are a team of professionals</p>-->

					<ul class="social-links text-center">
						<!-- Put your business social links  -->
<!--						<li><a href="#"><i class="fa fa-twitter fa-lg"></i></a></li>-->
<!--						<li><a href="#"><i class="fa fa-facebook fa-lg"></i></a></li>-->
<!--						<li><a href="#"><i class="fa fa-google-plus fa-lg"></i></a></li>-->
<!--						<li><a href="#"><i class="fa fa-dribbble fa-lg"></i></a></li>-->
					</ul>
				</div>
			</div>
			<!-- end single slide -->

		</div>
		<!-- End Wrapper for slides -->

	</div>
</section>