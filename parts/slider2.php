
<div class="path-slider-parent first-box">
		<div class="controls-tmpl controls" style="display: none;">
			<div class="control control-num">
				<div>
					<div>
						<div class="left">
							<div>
								<div class="check" style="display: none">
									<span></span>
									<input type="checkbox" checked />
								</div>
								<div class="txt1">1111</div>
								<div class="txt2">2222</div>
							</div>
						</div>
						<div class="right">
							<div class="line"></div>
							<input type="text" style="display: none">
							<img src="/img/ctrl-sel-btn.svg" style="display: none">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="path-slider path-line">
			<div></div>
		</div>
		<div class="start-stop">
			<div class="img start active">
				<svg x="0" y="0" width="100%" height="100%" viewBox="0 0 80 80" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					<g>
						<circle cx="40" cy="40" r="38"/>
						<path d="M25,20 L65,40 L25,60 Z"/>
					</g>
				</svg>
			</div>
			<div class="img pause">
				<svg x="0" y="0" width="100%" height="100%" viewBox="0 0 80 80" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					<g>
						<circle cx="40" cy="40" r="38"/>
						<path d="M25,20 l10,0 l0,40 l-10,0 Z"/>
						<path d="M45,20 l10,0 l0,40 l-10,0 Z"/>
					</g>
				</svg>
			</div>
		</div>
	</div>

	<!--div id="carousel-example-generic" class="carousel slide" data-ride="carousel">


		<ol class="carousel-indicators">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		</ol>



		<div class="carousel-inner" role="listbox">


			<div class="item active" style="background-image: url('');">
				<div class="carousel-caption">


					<h2 data-wow-duration="700ms" data-wow-delay="500ms" class="wow bounceInDown animated">Meet<span> Heydarah</span>!</h2>
					<h3 data-wow-duration="1000ms" class="wow slideInLeft animated"><span class="color">/creative</span> one page template.</h3>
					<p data-wow-duration="1000ms" class="wow slideInRight animated">We are a team of professionals</p>

					<ul class="social-links text-center">

						<li><a href="#"><i class="fa fa-twitter fa-lg"></i></a></li>
						<li><a href="#"><i class="fa fa-facebook fa-lg"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus fa-lg"></i></a></li>
						<li><a href="#"><i class="fa fa-dribbble fa-lg"></i></a></li>
					</ul>
				</div>
			</div>

		</div>


	</div -->